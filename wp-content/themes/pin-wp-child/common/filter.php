<?php
/**
 * Created by PhpStorm.
 * User: dam1r89
 * Date: 12/10/15
 * Time: 6:42 PM
 */
add_filter('posts_join', 'filter_search_join');
add_filter('posts_where', 'filter_search_where');
add_filter('posts_orderby', 'filter_order_by');
add_action('pre_get_posts', 'apply_custom_rules');


define('PRICE_MIN', 1);
define('PRICE_MAX', 2000);


add_action('wp_enqueue_scripts', 'filter_scripts');

function filter_scripts()
{
    wp_enqueue_script('filter', get_stylesheet_directory_uri() . '/common/js/filters.js');
    wp_enqueue_style('filter', get_stylesheet_directory_uri() . '/common/css/filters.css');
    wp_enqueue_style('jquery-ui', includes_url('css/jquery-ui.css'));
    wp_enqueue_script('jquery-ui-slider', false, ['jquery-ui-touch']);
    wp_enqueue_script('jquery-ui-touch', includes_url('shoptrends/jquery.ui.touch-punch.min.js'), ['jquery-ui-mouse', 'jquery-ui-widget']);

}


function apply_custom_rules($query)
{
    if ($query->is_main_query()) {

        if (get_input('categories') AND get_input('brands')) {

            $query->set('tax_query', [
                    [
                        'relation' => 'OR',
                        [
                            'taxonomy' => 'category',
                            'field' => 'term_id',
                            'terms' => get_input('categories'),
                        ],
                    ],
                    [
                        'relation' => 'OR',
                        [
                            'taxonomy' => 'category',
                            'field' => 'term_id',
                            'terms' => get_input('brands'),
                        ],
                    ]

                ]
            );
        } elseif (get_input('categories')) {
            $query->query_vars['category__in'] = get_input('categories');
        } elseif (get_input('brands')) {
            $query->query_vars['category__in'] = get_input('brands');
        }

    }
}

function filter_search_join($join)
{

    if (get_input('order') || get_input('price_high') || get_input('price_low')) {

        global $wpdb;
        $join .= sprintf(" LEFT JOIN %s m ON (%s.ID = m.post_id AND m.meta_key = '_salePrice') ",
            $wpdb->postmeta, $wpdb->posts
        );

        $join .= sprintf(" LEFT JOIN %s ms ON (%s.ID = ms.post_id AND ms.meta_key = '_price') ",
            $wpdb->postmeta, $wpdb->posts
        );

    }
    return $join;

}

function filter_search_where($where)
{

    if (get_input('price_low')) {
        $where .= sprintf(" and (  if(m.meta_value = '',ms.meta_value,m.meta_value) > %f ) ", get_input('price_low'));
    }
    if (get_input('price_high') && get_input('price_high') < PRICE_MAX) {
        $where .= sprintf(" and (  if(m.meta_value = '',ms.meta_value,m.meta_value) < %f ) ", get_input('price_high'));
    }

    return $where;

}

function filter_order_by($orderby_statement)
{

    if (get_input('order')) {
        if (strlen(trim($orderby_statement)) !== 0) {
            $orderby_statement = ", " . $orderby_statement;
        }
        $orderby_statement = sprintf(" CAST( IF(m.meta_value = '',ms.meta_value,m.meta_value) AS DECIMAL(7,2)) %s", get_input('order') == 'DESC' ? 'DESC' : 'ASC') . $orderby_statement;
        return $orderby_statement;

    }

    return $orderby_statement;
}
