<?php if (is_front_page()) : ?>

    <?php $pages = get_pages([
        'sort_column' => 'menu_order, post_title',
        'number' => 0,
        'include' =>  [130874,132540]
    ]); ?>
    <div class="featured-pages">
        <h2>Trendsetters - Discover up-and-coming bloggers around the world</h2>
        <ul class="child-pages">
            <?php foreach ($pages as $post): setup_postdata($post); ?>
                <li class="cf">
                    <a href="<?php the_permalink(); ?>" rel="nofollow"
                       class="child-image cf"> <?php the_post_thumbnail(); ?> </a>

                    <h3>
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </h3>

                    <p><?php echo anthemes_excerpt(strip_tags(strip_shortcodes(get_the_excerpt())), 200); ?><br>
                        <a href="<?php the_permalink(); ?>" rel="nofollow">Read more</a></p>
                </li>
            <?php endforeach; ?>
        </ul>
        <a href="http://stylecross.com/fashion-blogger-interviews/"><span>See all bloggers</span></a>
    </div>

<?php endif; ?>
