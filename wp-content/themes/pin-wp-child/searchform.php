<form id="searchform2" class="header-search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="submit" value="Search" class="buttonicon" />
    <input placeholder="<?php esc_html_e('Search over 400,000 products...', 'anthemes'); ?>" type="text" name="s" id="s" />
</form><div class="clear"></div>
